# Periódico frontend

Proyecto básico que emula un sitio web de noticias, el cúal cuenta con las siguientes funcionalidades:

- Una vista general para la visualización de artículos con estado PUBLICADO
- Pantalla de autenticación
- Dashboard básico para el editor de artículos
- Dashboard básico para el revisor de artículos


## Módo de ejecución

```sh
# instalar dependencias
npm i

# Iniciar la aplicación 
npm start
```
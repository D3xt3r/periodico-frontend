describe('template spec', () => {
  it('passes', () => {
    // login
    cy.visit('http://localhost:8000/login')
    cy.contains('Ingresar').should('be.visible')
    cy.get('#username').type('dev4')
    cy.get('#password').type('dev4')
    cy.get('[data-testid="acceder"]').click()
    cy.contains('Editor dashboard').should('be.visible')

    // crear artículo
    cy.get('[data-testid="crearArticulo"]').click()

    cy.get('[data-testid="guardarArticulo"]').should('be.visible')
    
    cy.get('[data-testid="titulo"]').type('Nuevo articulo UAB')
    cy.get('[data-testid="contenido"]').type('Lorem ipsum dolor at simet')
    cy.get('[data-testid="guardarArticulo"]').click()
    cy.contains('Editor dashboard').should('be.visible')

    // Validar existencia del articulo
    cy.contains('Nuevo articulo UAB').should('be.visible')
  })
})

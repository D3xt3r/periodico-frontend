describe('Suite autenticacion', () => {
  beforeEach(() => {
    cy.fixture('usuarios').as('usuarios')
  })

  it('deberia autenticar editor exitosamente', () => {
    cy.get('@usuarios').then((usuarios) => {
      cy.login(
        usuarios.editorValido.username,
        usuarios.editorValido.password
      )
    })
    cy.contains('Editor dashboard').should('be.visible')
  })

  it('deberia autenticar revisor exitosamente', () => {
    cy.get('@usuarios').then((usuarios) => {
      cy.login(usuarios.revisorValido.username, usuarios.revisorValido.password)
    })
    cy.contains('Reviewer dashboard').should('be.visible')
  })
})
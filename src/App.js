import './App.css';
import { BrowserRouter as Router, Route, Routes, Navigate } from 'react-router-dom';
import useAuthStore from './stores/useAuthStore';
import EditorDashboard from './components/Dashboard/Editor/EditorDashboard';
import ReviewerDashboard from './components/Dashboard/Reviewer/ReviewerDashboard';
import LandingPage from './components/LandingPage/LandingPage';
import Login from './components/Login/Login';
import Navbar from './components/Navbar/Navbar';
import { Container } from '@mui/material';

function App() {
  const {user} = useAuthStore()
  return (
    <Router>
      {user && <Navbar/>}
      <Container 
        style={{
          paddingBottom: '56px',
          paddingTop: '56px'
        }}
      >
        <Routes>
          <Route path='/' element={<LandingPage/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route
            path='/editor'
            element={user?.rol === 'editor' ? <EditorDashboard /> : <Navigate to="/" />}
          />
          <Route
            path='/revisor'
            element={user?.rol === 'revisor' ? <ReviewerDashboard /> : <Navigate to="/" />}
          />
        </Routes>
      </Container>
    </Router>
  );
}

export default App;

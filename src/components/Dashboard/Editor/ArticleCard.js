import React from 'react';
import { Card, CardContent, CardActions, Button, Typography, Avatar, Chip, IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import SendIcon from '@mui/icons-material/Send';
import VisibilityIcon from '@mui/icons-material/Visibility';
import CommentIcon from '@mui/icons-material/Comment';
import CheckIcon from '@mui/icons-material/Check'
import CloseIcon from '@mui/icons-material/Close'

const ArticleCard = ({ article, onEdit, onSubmitReview, onApprove, onReject }) => {
  const {
    titulo,
    cotenido,
    categoria,
    estado,
    views,
    comments,
    _fecha_creacion
  } = article;
  const colorChip = {
    'BORRADOR': 'primary',
    'OBSERVADO': 'error',
    'EN_REVISION': 'warning',
    'PUBLICADO': 'success',
  }
  return (
    <Card sx={{ maxWidth: 345, margin: '1em' }}>
      <CardContent>
        <Typography variant="h5" component="div">
          {titulo}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {cotenido}
        </Typography>
        <Chip label={categoria} />
        <Typography variant="body2" color="text.secondary">
          {_fecha_creacion}
        </Typography>
        {estado !== 'PUBLICADO' && <Chip label={estado} color={colorChip[estado]} />}
      </CardContent>
      <CardActions>
        {onSubmitReview && (
          <Button size="small" color="primary" startIcon={<SendIcon />} onClick={() => onSubmitReview(article.id)}>
            Enviar a Revisión
          </Button>
        )}
        {onEdit && (
          <Button size="small" color="primary" startIcon={<EditIcon />} onClick={() => onEdit(article)}>
            Editar
          </Button>
        )}
        {onApprove && (
          <Button size="small" color="primary" startIcon={<CheckIcon />} onClick={() => onApprove(article.id)}>
            Aprobar
          </Button>
        )}
        {onReject && (
          <Button size="small" color="secondary" startIcon={<CloseIcon />} onClick={() => onReject(article.id)}>
            Rechazar
          </Button>
        )}
        
        {/* {estado !== 'PUBLICADO' && (
          <Button size="small" color="primary" startIcon={<SendIcon />} onClick={() => onSubmitReview(article.id)}>
            Enviar a Revisión
          </Button>
        )}
        <Button size="small" color="primary" startIcon={<EditIcon />} onClick={() => onEdit(article)}>
          Editar
        </Button> */}
        <IconButton aria-label="views" disabled>
          <VisibilityIcon /> {views}
        </IconButton>
        <IconButton aria-label="comments" disabled>
          <CommentIcon /> {comments}
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default ArticleCard;

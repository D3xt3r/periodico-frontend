import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import useAuthStore from "../../../stores/useAuthStore"
import { Button, Container, Grid, Typography } from "@mui/material"
import ArticleCard from "./ArticleCard"
import EditArticleModal from "./EditArticleModal"
import api from "../../../services/api"
import Navbar from "../../Navbar/Navbar"

const EditorDashboard = () => {
  const [articles, setArticles] = useState([])
  const [editingArticle, setEditingArticle] = useState(null)
  const [modalOpen, setModalOpen] = useState(false)
  const navigate = useNavigate()
  const { user, logout } = useAuthStore()

  useEffect(() => {
    const fetchArticles = async () => {
      try {
        const response = await api.get('/articulos')
        
        setArticles(response.data.dato)
      } catch (error) {
        console.error('Error fetching articles')
      }
    }

    fetchArticles()
  },[])

  const handleEdit =  (article) => {
    setEditingArticle(article)
    setModalOpen(true)
  }

  const handleSave = async (updatedArticle) => {
    if (updatedArticle.id) {

      try {
        await api.put(`/articulos/${updatedArticle.id}`, updatedArticle)
        setArticles(articles.map((article) => (article.id === updatedArticle.id? updatedArticle: article)))
      } catch (error) {
        console.error('Error updating article', error)
      }
    } else {
      try {
        const response = await api.post(`/articulos`, updatedArticle)
        setArticles([...articles, response.data.dato])
      } catch (error) {
        console.error('Error updating article', error)
      }

    }
  }

  const handleSubmit = async (id) => {
    try {
      const updatedArticle = {estado: 'EN_REVISION'}
      
      await api.put(`/articulos/${id}`, updatedArticle)
      setArticles(articles.map((article) => (article.id === id ? {...article, estado: 'EN_REVISION'} : article)))

    } catch (error) {
      console.error('Error submitting article for review', error)
    }
  }

  const handleNewArticle = () => {
    setEditingArticle(null)
    setModalOpen(true)
  }
  return (
    <Container>
        <Typography variant="h4" gutterBottom>
          Editor dashboard
        </Typography>

      <Button 
        variant="contained"
        color="primary"
        onClick={handleNewArticle}
        data-testid="crearArticulo"
      >
        Crear Nuevo Artículo
      </Button>
      <Grid container spacing={3}>
        {articles.map((article) => (
          <Grid item key={article.id} xs={12} sm={6} md={4}>
            <ArticleCard
              article={article}
              onEdit={handleEdit}
              onSubmitReview={handleSubmit}
            />
          </Grid>
        ))}
      </Grid>
      {modalOpen && (
        <EditArticleModal
          open={modalOpen}
          onClose={() => setModalOpen(false)}
          onSave={handleSave}
          article={editingArticle}
        />
      )}
    </Container>
  )
}
export default EditorDashboard
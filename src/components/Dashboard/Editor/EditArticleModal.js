import React, { useEffect, useState } from 'react';
import { Modal, Box, TextField, Button, Select, MenuItem, InputLabel, FormControl } from '@mui/material';

const EditArticleModal = ({ open, onClose, onSave, article }) => {
  const [titulo, setTitulo] = useState(article?.titulo || '');
  const [contenido, setContenido] = useState(article?.contenido || '');
  const [categoria, setCategoria] = useState(article?.categoria || '');

  const handleSave = () => {
    onSave({
      ...article,
      titulo,
      contenido,
      categoria,
    });
    onClose();
  };

  useEffect(() => {
    if (article) {
      setTitulo(article.titulo)
      setContenido(article.contenido)
      setCategoria(article.categoria)
      } else {
      setTitulo('')
      setContenido('')
      setCategoria('')

    }
  }, [article])
  return (
    <Modal open={open} onClose={onClose}>
      <Box sx={{ p: 4, backgroundColor: 'white', margin: 'auto', maxWidth: 500, mt: 5 }}>
        <TextField
          fullWidth
          label="Title"
          value={titulo}
          onChange={(e) => setTitulo(e.target.value)}
          margin="normal"
          data-testid="titulo"
        />
        <TextField
          fullWidth
          label="Content"
          value={contenido}
          onChange={(e) => setContenido(e.target.value)}
          margin="normal"
          multiline
          rows={4}
          data-testid="contenido"
        />
        <FormControl fullWidth margin="normal">
          <InputLabel>Category</InputLabel>
          <Select value={categoria} onChange={(e) => setCategoria(e.target.value)}>
            <MenuItem value="Tecnología">Tecnología</MenuItem>
            <MenuItem value="Social">Social</MenuItem>
            <MenuItem value="Salud">Salud</MenuItem>
          </Select>
        </FormControl>
        <Button 
          data-testid="guardarArticulo"
          variant="contained"
          color="primary"
          onClick={handleSave}
        >
          Save
        </Button>
      </Box>
    </Modal>
  );
};

export default EditArticleModal;

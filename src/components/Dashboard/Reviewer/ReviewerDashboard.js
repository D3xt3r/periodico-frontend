import React, { useEffect, useState } from "react"
import { Container, Grid, Typography } from "@mui/material"
import ArticleCard from "../Editor/ArticleCard"
import api from "../../../services/api"



const ReviewerDashboard = () => {
  const [articles, setArticles] = useState([])

  useEffect(() => {
    const fetchArticles = async () => {
      try {
        const response = await api.get('/articulos')
        const result = await filtrar(response.data.dato || [])
        setArticles(result)
      } catch (error) {
        console.error('Error fetching articles', error)
      }
    }
    fetchArticles()
  }, [])


  const filtrar = async (list) => {
    const result = []
    for (let index = 0; index < list.length; index++) {
      const article = list[index]
      if (article.estado === 'EN_REVISION') {
        result.push(article)
      }
    }
    return result
  }
  const handleApprove = async (id) => {
    try {
      await api.put(`/articulos/${id}`, {estado: 'PUBLICADO'})
      setArticles(articles.filter((article) => article.id !== id))
    } catch (error) {
      console.error('Error rejecting article')
    }
  }
  const handleReject = async (id) => {
    try {
      await api.put(`/articulos/${id}`, {estado: 'OBSERVADO'})
      setArticles(articles.filter((article) => article.id !== id))
    } catch (error) {
      console.error('Error rejecting article')
    }
  }
  return (
    <Container>
      <Typography variant="h4" gutterBottom>
        Reviewer dashboard
      </Typography>
      <Grid container spacing={3}>
        {articles.map((article) => (
          <Grid item key={article.id} xs={12} sm={6} md={4}>
            <ArticleCard
              article={article}
              onEdit={null}
              onSubmitReview={null}
              onApprove={handleApprove}
              onReject={handleReject}
            />
          </Grid>
        ))}
      </Grid>
    </Container>
  )
}
export default ReviewerDashboard
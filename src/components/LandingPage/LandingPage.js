import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import api from "../../services/api"
import { Button, Card, CardActions, CardContent, Container, Grid, Typography } from "@mui/material"

const LandingPage = () => {
  const [articles, setArticles] = useState([])

  useEffect(() => {
    const fetchArticles = async () => {
      try {
        const response = await api.get('/articulos')
        setArticles(response.data.dato)
      } catch (error) {
        console.error('Error fetching articles', error)
      }
    }

    fetchArticles()
  },[])

  return (
    <Container>
      <Typography variant="h3" component="h1" gutterBottom>
        Artículos publicados
      </Typography>
      <Grid container spacing={4}>
        {articles.map((article) => (
          <Grid item key={article.id} xs={12} sm={6} md={4}>
            <Card>
              <CardContent>
                <Typography variant="h5" component="h2">
                  {article.titulo}
                </Typography>
                <Typography>
                  {article.contenido}
                </Typography>
              </CardContent>
              <CardActions>
                <Button 
                  size="small"
                  color="primary"
                  component={Link}
                  to={`/articulos/${article.id}`}
                >
                  Ver detalles
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
      <Button
        sx={{mt:3}}
        variant="contained"
        color="primary"
        component={Link}
        to="/login"
        // id="acceder"
        data-testid="button-acceder"
      >
        Acceder
      </Button>
      {/* <Button
        sx={{mt:3}}
        variant="contained"
        color="primary"
        component={Link}
        to="/login"
        // id="acceder"
        // data-testid="button-acceder"
      >
        Acceder2
      </Button> */}
    </Container>
  )
}
export default LandingPage
import React, { useState } from "react"
import useAuthStore from "../../stores/useAuthStore"
import { useNavigate } from "react-router-dom"
import { 
  Avatar,
  Box,
  Button,
  Container,
  CssBaseline,
  Grid,
  Link,
  TextField,
  Typography
} from "@mui/material"
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';

const Login = () => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const login = useAuthStore((state) => state.login)
  const navigate = useNavigate()

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      await login(username, password)
      const { user } = useAuthStore.getState()
      console.log('revisando el login', user)
      if (user.rol === 'editor') {
        navigate('/editor')
      }
      else if (user.rol === 'revisor') {
        navigate('/revisor')

      }
    } catch (error) {
      console.error('Login failed', error)
    }
  }


  return (
    <Container>

      <CssBaseline/>
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: 'secondary.main'}}>
          <LockOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h5">
          Ingresar
        </Typography>

        <Box component="form" onSubmit={handleSubmit} noValidate sx={{mt:1}}>
          <TextField
            margin="normal"
            required
            fullWidth
            name="username"
            label="username"
            type="username"
            id="username"
            autoComplete="username"
            autoFocus
            value={username}
            onChange={(event) => setUsername(event.target.value)}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
          />
          <Button
            type="submit"
            fullWidth
            data-testid="acceder"
            variant="contained"
            sx={{mt:3, mb:2}}
          >Ingresar</Button>
          <Grid container>
            <Grid item>
              <Link href="#" variant="body2">
                {"No tienes una cuenta aun? Registrate"}
              </Link>
            </Grid>
          </Grid>
        </Box>

      </Box>

    </Container>
  )
}

export default Login
import axios from "axios";
import { create } from "zustand";
import { persist } from "zustand/middleware";

const useAuthStore = create(
  persist(
    (set) => ({
      user: null,
      login: async (username, password) => {
        const response = await axios.post('http://localhost:3000/auth/login', { username, password })
        set({ user: response.data })
      },
      logout: () => set({ user: null }),
    }),
    {name: 'auth'}
  )
)
export default useAuthStore
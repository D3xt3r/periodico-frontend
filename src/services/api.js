import axios from "axios";
import useAuthStore from "../stores/useAuthStore";

// console.log('UseAuthStore', useAuthStore)
const api = axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: true,
})


api.interceptors.request.use(
  (config) => {
    console.log('[interceptor] start')
    const access_token = useAuthStore.getState().user?.access_token;
    console.log('[interceptor] access_token', access_token)
    if (access_token) {
      config.headers['Authorization'] = `Bearer ${access_token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default api